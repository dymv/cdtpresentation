//
//  CDTAppDelegate.h
//  CDTDemo
//
//  Created by Eugene Dymov on 07.03.13.
//  Copyright (c) 2013 Mobile Up. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CDTViewController;

@interface CDTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *viewController;

@end
