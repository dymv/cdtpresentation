//
// Created by eugenedymov on 13.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "CDTArtistCell.h"

@interface CDTArtistCell ()
@end

@implementation CDTArtistCell
{
    NSNumberFormatter *_nf;
}

- (id)initWithStyle: (UITableViewCellStyle)style
    reuseIdentifier: (NSString *)reuseIdentifier
{
    self = [super initWithStyle: UITableViewCellStyleSubtitle
                reuseIdentifier: reuseIdentifier];
    if ( self )
    {
    }

    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.textLabel.text = nil;
    self.detailTextLabel.text = nil;
}

- (void)setName: (NSString *)name
{
    self.textLabel.text = name;
}

- (void)setListenersCount: (NSNumber *)listenersCount
{
    self.detailTextLabel.text = [self _formatListenersCount: listenersCount];
}

- (NSString *)_formatListenersCount:(NSNumber *)lc
{
    return [[self _nf] stringFromNumber: lc];
}

- (NSNumberFormatter *)_nf
{
    if (!_nf) {
        _nf = [[NSNumberFormatter alloc] init];
        [_nf setNumberStyle: NSNumberFormatterDecimalStyle];
    }

    return _nf;
}

- (void)dealloc
{
    [_nf release];
    [super dealloc];
}


@end