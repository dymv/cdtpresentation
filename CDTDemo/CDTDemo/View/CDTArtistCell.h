//
// Created by eugenedymov on 13.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface CDTArtistCell : UITableViewCell
- (void)setName:(NSString *)name;
- (void)setListenersCount:(NSNumber *)listenersCount;
@end