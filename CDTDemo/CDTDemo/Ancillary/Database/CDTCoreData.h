//
// Created by eugenedymov on 11.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface CDTCoreData : NSObject

@property (nonatomic, retain, readonly) NSManagedObjectContext *context;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *coordinator;

+ (CDTCoreData *)sharedInstance;

- (void)insertArtistWithName:(NSString *)name url:(NSString *)url listenersCount:(NSInteger)listenersCount imageURL:(NSString *)imageURL;

- (void)insertArtistWithName:(NSString *)name url:(NSString *)url listenersCount:(NSInteger)listenersCount imageURL:(NSString *)imageURL context:(NSManagedObjectContext *)moc;

- (void)save;

- (void)dropArtists;

- (NSArray *)getArtists;

@end