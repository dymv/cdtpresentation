//
// Created by eugenedymov on 11.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import "CDTCoreData.h"
#import "CDTArtistEntity.h"

@interface CDTCoreData ()
@property (nonatomic, retain) NSManagedObjectModel *model;
@property (nonatomic, retain) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, retain) NSManagedObjectContext *context;
@end

@implementation CDTCoreData
{

}

+ (CDTCoreData *)sharedInstance
{
    static dispatch_once_t once;
    static CDTCoreData *sharedInstance;
    dispatch_once(
        & once, ^
        {
            sharedInstance = [[CDTCoreData alloc] init];
        }
);
    return sharedInstance;
}

- (void)insertArtistWithName: (NSString *)name
                         url: (NSString *)url
              listenersCount: (NSInteger)listenersCount
                    imageURL: (NSString *)imageURL
{
    [self insertArtistWithName: name
                           url: url
                listenersCount: listenersCount
                      imageURL: imageURL
                       context: self.context];
}

- (void)insertArtistWithName: (NSString *)name
                         url: (NSString *)url
              listenersCount: (NSInteger)listenersCount
                    imageURL: (NSString *)imageURL
                     context: (NSManagedObjectContext *)moc
{
    CDTArtistEntity *artistEntity = [NSEntityDescription insertNewObjectForEntityForName: @"CDTArtist"
                                                                  inManagedObjectContext: moc];

    artistEntity.name = name;
    artistEntity.url = url;
    artistEntity.listenersCount = [NSNumber numberWithInteger: listenersCount];
    artistEntity.imageURL = imageURL;

    NSError *error = nil;

    if ( ! [moc save: & error] )
    {
        NSLog(@"CDTCoreData.insertArtist\nStatus:\tSaveFailed\nError:\t%@", error);
    }
}


- (void)save
{
    if ( ! self.context.hasChanges )
    {
        return;
    }
    NSError *error = nil;

    if ( ! [self.context save: & error] )
    {
        NSLog(@"CDTCoreData.Save\nStatus:\tFail\nError:\t%@", error);
    }
}

- (void)dropArtists
{
    [self dropTable: @"CDTArtist"];
}

- (NSArray *)getArtists
{
    return [self readEntitiesFromTable: @"CDTArtist"];
}


- (void)dropTable:(NSString *)tableName {
    if ( ! tableName.length ) {
        return;
    }
    NSManagedObjectContext *context = [self context];
    NSArray *entities = [self readEntitiesFromTable:tableName];

    for (NSManagedObject *object in entities) {
        [context deleteObject:object];
    }

    [self save];
}

- (NSArray *)readEntitiesFromTable:(NSString *)tableName {
    if ( ! tableName.length ) {
        return nil;
    }

    NSManagedObjectContext *context = [self context];

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:tableName];
    NSSortDescriptor *listenersCountSort = [[NSSortDescriptor alloc] initWithKey:@"listenersCount" ascending:NO selector:nil];
    [fetchRequest setSortDescriptors: @[listenersCountSort]];

    NSError *error = nil;
    NSArray *entities = [context executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];

    if ( error ) {
        NSLog(@"Error while reading entities: %@", [error localizedDescription]);
        return nil;
    }

    [listenersCountSort release];

    return entities;
}


- (id)init
{
    self = [super init];
    if ( self )
    {
        [self _init];
    }

    return self;
}

- (void)_init
{
    [self _setupCoreDataStack];
}

- (void)_setupCoreDataStack
{
    self.model = [self _model];
    NSURL *storeURL = [NSURL fileURLWithPath: [[self _applicationCacheDirectory] stringByAppendingPathComponent: @"CDTBase.sqlite"]];
    NSLog(@"setupCoreDataStack\nStoreURL:\t%@", storeURL);

    self.coordinator = [[[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: self.model] autorelease];

    [self _addPersistentStoreWithURL: storeURL onCompletion: ^(BOOL completed, NSError *error)
    {
        NSLog(@"addPersistentStore\nCompleted:\t%d\nError:\t%@", completed, error);

        if ( ! completed )
        {
            [[NSFileManager defaultManager] removeItemAtURL: storeURL error: NULL];

            [self _addPersistentStoreWithURL: storeURL onCompletion: ^(BOOL nestCompleted, NSError *nestError)
            {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }];
        }
        else
        {

            self.context = [[[NSManagedObjectContext alloc] init] autorelease];
            [self.context setPersistentStoreCoordinator: self.coordinator];

        }
    }];
}

- (void)_addPersistentStoreWithURL: (NSURL *)url
                      onCompletion: (void (^)(BOOL completed, NSError *error))completion
{
    NSError *error = nil;
    BOOL completed = ! ! [self.coordinator addPersistentStoreWithType: NSSQLiteStoreType configuration: nil URL: url options: nil error: & error];

    if ( completion )
    {
        completion(completed, error);
    }
}

- (NSString *)_applicationCacheDirectory
{
    NSArray *cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [cachePathArray lastObject];

    return cachePath;
}

- (NSManagedObjectModel *)_model
{
    NSEntityDescription *entity = [[NSEntityDescription alloc] init];
    [entity setName: @"CDTArtist"];
    [entity setManagedObjectClassName: @"CDTArtist"];

    NSMutableArray *properties = [NSMutableArray array];

    NSAttributeDescription *name = [[NSAttributeDescription alloc] init];
    [name setName: @"name"];
    [name setAttributeType: NSStringAttributeType];
    [name setOptional: NO];
    [name setIndexed: YES];

    NSAttributeDescription *url = [[NSAttributeDescription alloc] init];
    [url setName: @"url"];
    [url setAttributeType: NSStringAttributeType];
    [url setOptional: NO];

    NSAttributeDescription *listenersCount = [[NSAttributeDescription alloc] init];
    [listenersCount setName: @"listenersCount"];
    [listenersCount setAttributeType: NSInteger32AttributeType];
    [listenersCount setOptional: YES];
    [listenersCount setDefaultValue: @0];

    NSAttributeDescription *imageURL = [[NSAttributeDescription alloc] init];
    [imageURL setName: @"imageURL"];
    [imageURL setAttributeType: NSStringAttributeType];
    [imageURL setOptional: YES];
    [imageURL setDefaultValue: @""];

    [properties addObjectsFromArray: @[
            name,
            url,
            listenersCount,
            imageURL
    ]];

    [entity setProperties: properties];

    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] init];
    [model setEntities: @[entity]];

    [name release];
    [entity release];
    [url release];
    [imageURL release];
    [listenersCount release];

    return [model autorelease];
}


@end