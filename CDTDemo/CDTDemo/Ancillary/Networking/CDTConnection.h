//
// Created by eugenedymov on 11.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

typedef void (^CDTConnectionCompletionBlock_t) (NSData *data, NSError *error);

@interface CDTConnection : NSObject
+ (id)connection;
- (void)getDataFromURL:(NSURL *)url onCompletion:(CDTConnectionCompletionBlock_t)completion;
@end