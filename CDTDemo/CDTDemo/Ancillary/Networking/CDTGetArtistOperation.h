//
// Created by eugenedymov on 11.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@interface CDTGetArtistOperation : NSOperation
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, copy) dispatch_block_t onCompletion;

- (id)initWithPage: (NSInteger)page;

@end