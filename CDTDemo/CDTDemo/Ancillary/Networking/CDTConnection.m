//
// Created by eugenedymov on 11.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "CDTConnection.h"

@interface CDTConnection ()
@property (nonatomic, copy) CDTConnectionCompletionBlock_t completion;
@end

@implementation CDTConnection
{
}

+ (id)connection
{
    return [[[CDTConnection alloc] init] autorelease];
}

- (void)getDataFromURL: (NSURL *)url
          onCompletion: (CDTConnectionCompletionBlock_t)completion
{
    self.completion = completion;

    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = nil;

    data = [NSURLConnection sendSynchronousRequest: [NSURLRequest requestWithURL: url
                                                                     cachePolicy: NSURLRequestReloadIgnoringCacheData
                                                                 timeoutInterval: 60.0]
                                 returningResponse: & response
                                             error: & error];


    self.completion(data, error);

}

@end