//
// Created by eugenedymov on 11.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

typedef void (^CDTAPIClientCompletionBlock_t)(NSTimeInterval elapsed, NSError *error);
typedef void (^CDTAPIClientProgressBlock_t)(CGFloat progress);

@interface CDTAPIClient : NSObject

+ (CDTAPIClient *)sharedInstance;

- (void)getArtistsSerially: (BOOL)isSerially
              onCompletion: (CDTAPIClientCompletionBlock_t)completion
                onProgress: (CDTAPIClientProgressBlock_t)progressBlock;

- (void)cancelAll;
@end