//
// Created by eugenedymov on 11.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "CDTAPIClient.h"
#import "CDTConnection.h"
#import "CDTGetArtistOperation.h"
#import "Defines.h"
#import "CDTCoreData.h"

@interface CDTAPIClient ()
@property (nonatomic, retain) NSOperationQueue *queue;
@end

@implementation CDTAPIClient
{

}

+ (CDTAPIClient *)sharedInstance {
    static dispatch_once_t once;
    static CDTAPIClient *sharedInstance;
    dispatch_once(&once, ^ { sharedInstance = [[CDTAPIClient alloc] init]; });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if ( self )
    {
        self.queue = [[[NSOperationQueue alloc] init] autorelease];
    }

    return self;
}


- (void)getArtistsSerially: (BOOL)isSerially
              onCompletion: (CDTAPIClientCompletionBlock_t)completion
                onProgress: (CDTAPIClientProgressBlock_t)progressBlock
{
    [self cancelAll];
    [self.queue setMaxConcurrentOperationCount: isSerially ? 1 : kCDTConcurrentOperations];
    [CD dropArtists];

    NSDate *startDate = [NSDate date];
    const NSInteger toDownload = kCDTPagesToDownload;
    __block NSInteger downloaded = 0;

    for ( int i = 1; i <= toDownload; i ++ )
    {
        CDTGetArtistOperation *op = [[CDTGetArtistOperation alloc] initWithPage: i];

        [op setOnCompletion: ^
        {
            downloaded++;
            float p = 1.0 * downloaded / toDownload;

            NSLog(@"%d/%d", downloaded, toDownload);

            if ( progressBlock )
            {
                progressBlock(p);
            }

            if ( downloaded >= toDownload )
            {
                NSTimeInterval elapsed = [[NSDate date] timeIntervalSinceDate: startDate];

                if ( completion )
                {
                    completion(elapsed, nil);
                }
            }
        }];

        [self.queue addOperation: op];

        [op release];
    }
}

- (void)cancelAll
{
    [self.queue cancelAllOperations];
}


@end