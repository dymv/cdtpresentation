//
// Created by eugenedymov on 11.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "CDTGetArtistOperation.h"
#import "AFHTTPRequestOperation.h"
#import "CDTConnection.h"
#import "JSONKit.h"
#import "Defines.h"
#import "CDTCoreData.h"

@interface CDTGetArtistOperation ()
@property (nonatomic, retain) CDTConnection *connection;
@property (nonatomic, assign) BOOL executing;
@property (nonatomic, assign) BOOL finished;
@property (nonatomic, assign) BOOL cancelled;
@property(nonatomic, retain) NSManagedObjectContext *ctx;
@end

@implementation CDTGetArtistOperation
{
}
- (id)initWithPage: (NSInteger)page
{
    self = [super init];
    if ( self )
    {
        self.page = page;
        self.cancelled = NO;
        self.executing = NO;
        self.finished = NO;
    }

    return self;
}

- (void)dealloc
{
    [self removeRegistrationFromNotificationCenter];
    [super dealloc];
}

- (BOOL)isExecuting
{
    return self.executing;
}

- (BOOL)isFinished
{
    return self.finished;
}

- (BOOL)isCancelled
{
    return self.cancelled;
}

- (void)cancel
{
    [self _becomeCancelled];
}

- (void)main
{
    if ( self.isCancelled )
    {
        [self _stopExecuting];
        return;
    }

#if kCDTConcurrentDownloadFixed
    [self _createAndSetupContext];
    [self _registerContextWithNotificationCenter];
#endif

    [self _startExecuting];
    [self _getUsingCDTConnection];
}

#pragma mark - Private Methods

- (void)_getUsingCDTConnection
{
    self.connection = [CDTConnection connection];

    void (^ch) (NSData *, NSError *) = ^(NSData *data, NSError *error)
    {
        NSString *responseString = [[NSString alloc] initWithData: data
                                                         encoding: NSUTF8StringEncoding];

        NSDictionary *responseDictionary = [responseString objectFromJSONString];
        [responseString release];

        NSArray *artists = [responseDictionary valueForKeyPath: @"results.artistmatches.artist"];
        [self _parseArtists: artists];

        [self _becomeFinished];
    };

    [self.connection getDataFromURL: [self _url]
                       onCompletion: ch];
}

- (void)_parseArtists: (NSArray *)artistsArray
{
    if ( self.isCancelled )
    {
        [self _stopExecuting];
        return;
    }

    for ( NSDictionary *artist in artistsArray )
    {
        NSString *name = artist[@"name"];
        NSString *url = artist[@"url"];
        NSString *listenersCountString = artist[@"listeners"];
        NSInteger listenersCount = listenersCountString.integerValue;
        NSString *imageURL = [artist[@"image"] objectAtIndex: 1][@"#text"];

        NSManagedObjectContext *moc = [CD context];

#if kCDTConcurrentDownloadFixed
        moc = self.ctx;
#endif

        [CD insertArtistWithName: name
                             url: url
                  listenersCount: listenersCount
                        imageURL: imageURL
                         context: moc];

    }
}

- (void)_startExecuting
{
    [self willChangeValueForKey: @"isExecuting"];
    self.executing = YES;
    [self didChangeValueForKey: @"isExecuting"];
}

- (void)_stopExecuting
{
    [self willChangeValueForKey: @"isExecuting"];
    [self willChangeValueForKey: @"isFinished"];
    self.executing = NO;
    self.finished = YES;
    [self didChangeValueForKey: @"isExecuting"];
    [self didChangeValueForKey: @"isFinished"];
}

- (void)_becomeFinished
{
    if ( self.onCompletion )
    {
        dispatch_async(
                dispatch_get_main_queue(), ^
                                           {
                                               self.onCompletion();
                                           }
        );
    }
    [self _stopExecuting];
}

- (void)_becomeCancelled
{
    [self willChangeValueForKey: @"isExecuting"];
    [self willChangeValueForKey: @"isCancelled"];
    self.executing = NO;
    self.cancelled = YES;
    [self didChangeValueForKey: @"isExecuting"];
    [self didChangeValueForKey: @"isCancelled"];
}

- (NSString *)_urlString
{
    return [NSString stringWithFormat: @"http://ws.audioscrobbler.com/2.0/?method=artist.search&artist=the&api_key=a24e9d4d8e4cdb611e5ee9630d0aaeb6&format=json&page=%d", self.page];
}

- (NSURL *)_url
{
    return [NSURL URLWithString: [self _urlString]];
}

#pragma mark - Core Data Stuff

- (void)_createAndSetupContext
{
    NSPersistentStoreCoordinator *coordinator = [CD coordinator];

    self.ctx = [[[NSManagedObjectContext alloc] init] autorelease];
    [self.ctx setPersistentStoreCoordinator:coordinator];
    [self.ctx setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
}

- (void)_registerContextWithNotificationCenter
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(mergeChanges:)
               name:NSManagedObjectContextDidSaveNotification
             object:self.ctx];
}

- (void)removeRegistrationFromNotificationCenter {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:NSManagedObjectContextDidSaveNotification object:self.ctx];
}

- (void)mergeChanges:(NSNotification *)notification
{
    NSManagedObjectContext *mainContext = [CD context];

    // Merge changes into the main context on the main thread
    [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:)
                                  withObject:notification
                               waitUntilDone:YES];
}


@end