#ifndef CDT_DEMO_DEFINES
#define CDT_DEMO_DEFINES

#define kCDTConcurrentDownloadFixed 1
#define kCDTPagesToDownload 32
#define kCDTConcurrentOperations 8

#define CD [CDTCoreData sharedInstance]

typedef enum {
    CDTArtistViewControllerTypeSerial,
    CDTArtistViewControllerTypeConcurrent
} CDTArtistViewControllerType_t;

#endif