//
//  main.m
//  CDTDemo
//
//  Created by Eugene Dymov on 07.03.13.
//  Copyright (c) 2013 Mobile Up. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CDTAppDelegate class]));
    }
}
