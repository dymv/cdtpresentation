//
//  CDTViewController.m
//  CDTDemo
//
//  Created by Eugene Dymov on 07.03.13.
//  Copyright (c) 2013 Mobile Up. All rights reserved.
//

#import "CDTViewController.h"
#import "CDTArtistViewController.h"

static NSString *cellId = @"kCDTVCCell";

@interface CDTViewController ()
@property (nonatomic, retain) NSArray *titles;
@end

@implementation CDTViewController

- (id)init
{
    self = [super init];
    if ( self )
    {
        [self _init];
    }

    return self;
}

- (void)_init
{
    self.titles = @[NSLocalizedString(@"kMainViewControllerSerialCellLabel", @"Serial"), NSLocalizedString(@"kMainViewControllerConcurrentCellLabel", @"Concurrent")];
    [self.tableView registerClass: [UITableViewCell class] forCellReuseIdentifier: cellId];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = NSLocalizedString(@"kMainViewControllerTitle", @"CDT");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView: (UITableView *)tableView
 numberOfRowsInSection: (NSInteger)section
{
    return self.titles.count;
}

- (UITableViewCell *)tableView: (UITableView *)tableView
         cellForRowAtIndexPath: (NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellId forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = self.titles[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)      tableView: (UITableView *)tableView
didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    CDTArtistViewController *avc = [CDTArtistViewController objectWithType: (CDTArtistViewControllerType_t) indexPath.row];
    [self.navigationController pushViewController: avc animated: YES];
}


@end
