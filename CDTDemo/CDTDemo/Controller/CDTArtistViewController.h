//
// Created by eugenedymov on 11.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "Defines.h"

@interface CDTArtistViewController : UITableViewController

@property (nonatomic, assign) CDTArtistViewControllerType_t type;

- (id)initWithType: (CDTArtistViewControllerType_t)type;
+ (id)objectWithType: (CDTArtistViewControllerType_t)type;

@end