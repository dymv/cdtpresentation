//
// Created by eugenedymov on 11.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "CDTArtistViewController.h"
#import "CDTAPIClient.h"
#import "CDTCoreData.h"
#import "CDTArtistEntity.h"
#import "CDTArtistCell.h"

static NSString *cellId = @"kCDTVArtistCCell";

@interface CDTArtistViewController ()
@property (nonatomic, retain) NSArray *artists;
@property (nonatomic, retain) UIProgressView *pv;
@property (nonatomic, retain) UILabel *el;
@end

@implementation CDTArtistViewController
{

}
- (id)initWithType: (CDTArtistViewControllerType_t)type
{
    self = [super init];
    if ( self )
    {
        self.type = type;
        [self.tableView registerClass: [CDTArtistCell class] forCellReuseIdentifier: cellId];
    }

    return self;
}

+ (id)objectWithType: (CDTArtistViewControllerType_t)type
{
    return [[[CDTArtistViewController alloc] initWithType: type] autorelease];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.refreshControl = [[UIRefreshControl new] autorelease];
    [self.refreshControl addTarget: self
                            action: @selector(refreshControlAction)
                  forControlEvents: UIControlEventValueChanged];

    self.pv = [self _createProgressView];
    self.el = [self _createLabel];

    [self.pv setHidden: YES];
    [self.el setHidden: YES];

    self.navigationItem.titleView = [self _createTitleView];
}

- (void)viewWillDisappear: (BOOL)animated
{
    [super viewWillDisappear: animated];
    [[CDTAPIClient sharedInstance] cancelAll];
}

- (void)refreshControlAction
{
    [self _prepareForRefresh];

    BOOL isSerial = self.type == CDTArtistViewControllerTypeSerial;

    [[CDTAPIClient sharedInstance] getArtistsSerially: isSerial
                                         onCompletion: ^(NSTimeInterval elapsed, NSError *error)
                                         {
                                             [self _handleCompleteRefreshingWithTimeElapsed: elapsed];
                                         }
                                           onProgress: ^(CGFloat progress)
                                           {
                                               [self _updateProgress: progress];
                                           }];

    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView: (UITableView *)tableView
 numberOfRowsInSection: (NSInteger)section
{
    return self.artists.count;
}

- (UITableViewCell *)tableView: (UITableView *)tableView
         cellForRowAtIndexPath: (NSIndexPath *)indexPath
{
    CDTArtistCell *cell = [tableView dequeueReusableCellWithIdentifier: cellId
                                                          forIndexPath: indexPath];

    CDTArtistEntity *artist = self.artists[indexPath.row];
    [cell setName: artist.name];
    [cell setListenersCount: artist.listenersCount];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)      tableView: (UITableView *)tableView
didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath
                             animated: YES];

    CDTArtistEntity *artist = self.artists[indexPath.row];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: artist.url]];
}

#pragma mark - Private Methods

- (void)_prepareForRefresh
{
    [self.pv setProgress: 0.0];
    [self.pv setHidden: NO];
    [self.el setHidden: YES];
}

- (void)_reload
{
    self.artists = [CD getArtists];
    [self.tableView reloadData];
}

- (void)_handleCompleteRefreshingWithTimeElapsed:(NSTimeInterval)elapsed
{
    [self.pv setHidden: YES];
    [self.el setHidden: NO];
    self.el.text = [NSString stringWithFormat: @"%.1fs", elapsed];
    [self.refreshControl endRefreshing];
    [self _reload];
}

- (void)_updateProgress:(float)progress
{
    [self.pv setProgress: progress
                animated: YES];
}

- (UILabel *)_createLabel
{
    UILabel *label = [[UILabel alloc] init];
    [label setBackgroundColor: [UIColor clearColor]];
    [label setTextAlignment: NSTextAlignmentCenter];
    [label setTextColor: [UIColor whiteColor]];
    [label setFont: [UIFont fontWithName: @"HelveticaNeue-Light"
                                      size: 14]];
    [label setShadowColor: [UIColor colorWithWhite: 0.0
                                               alpha: 0.3]];
    [label setShadowOffset: CGSizeMake(0, 1)];
    [label setAutoresizingMask: UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];

    return [label autorelease];
}

- (UIProgressView *)_createProgressView
{
    UIProgressView *progressView = [[UIProgressView alloc] init];
    [progressView setProgressViewStyle: UIProgressViewStyleBar];
    [progressView setAutoresizingMask: UIViewAutoresizingFlexibleWidth];
    return [progressView autorelease];
}

- (UIView *)_createTitleView
{
    UIView *view = [[UIView alloc] init];
    [view setFrame: CGRectMake(0, 0, 100, 40)];
    [self.pv setFrame: CGRectMake(0, floorf((view.frame.size.height - self.pv.frame.size.height)/2.0), view.frame.size.width, self.pv.frame.size.height)];
    [view addSubview: self.pv];
    [self.el setFrame: view.bounds];
    [view addSubview: self.el];

    return [view autorelease];
}


@end