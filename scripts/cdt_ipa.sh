#!/bin/bash

CERT="iPhone Distribution: MobileUp o.o.o."
PROV="$PWD/../provision/Super-AdHoc.mobileprovision"
TARGET_SUFFIX=""
IPA_SUFFIX=""
IPA_DIR="Main"
PROJ="CDTDemo"

echo "Building.."
IPA_PATH=`./build.sh "$PROJ" "$PROJ" "${CERT}" "${PROV}" "CDT" "${IPA_DIR}"`

if [ $? == 0 ]
then
	echo "[OK]"
else
	echo "Build failed" 1>&2
	exit 1
fi

echo "Uploading.."
./testflight_upload.sh $IPA_PATH

if [ $? == 0 ]
then
	echo "Uploading [OK]"
else 
	echo "Uploading [FAILED]" 1>&2
fi

exit 0 
