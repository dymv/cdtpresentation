#!/bin/bash
#TODO: copy dSYM to IPA folder
# $1 — Project dir
# $2 — Target name
# $3 — Distribution Certificate name
# $4 — Provision Profile name
# $5 — IPA name
# $6 — IPA dir

PROJDIR="../$1/"
BUILDDIR="${HOME}/Documents/Builds/CDTOBJ"

if [ ! -d "$BUILDDIR" ]; then
	mkdir -p "$BUILDDIR"
fi

IPADIR="${HOME}/Documents/Builds/CDTIPA"

if [ ! -d "$IPADIR" ]; then
	mkdir -p "$IPADIR"
fi

PROJECT_NAME="$1"
TARGET_SDK="iphoneos6.1"
BUILDLOG="$1_build_log.txt"

if [ ! -z "$6" ]; then
	IPADIR="${IPADIR}/$6"
fi

# Trying to get target name, if no — using project name
if [ -z $2 ]; then
	TARGETNAME=${PROJECT_NAME}
else
	TARGETNAME=$2
fi

#
cd ${PROJDIR}

# Cleaning build log
cat /dev/null > ${BUILDLOG}

# Action!
xcodebuild -workspace "${PROJECT_NAME}.xcworkspace" -scheme "${TARGETNAME}" -sdk "${TARGET_SDK}" -configuration Release OBJROOT=$BUILDDIR SYMROOT=$BUILDDIR >> ${BUILDLOG} 2>&1

#Check if build succeeded
if [ $? != 0 ]
then
	echo "Building of $1 was not succeed. Check ${BUILDLOG}" 1>&2
	exit 1
fi

PROJECT_BUILDDIR="${BUILDDIR}/Release-iphoneos"
# TODO: make config files
DISTRIBUTION_CERTIFICATE="iPhone Distribution: MobileUp o.o.o."
PROVISONNING_PROFILE="${HOME}/Documents/Provision/Super-AdHoc.mobileprovision"

#
if [ -z "$3" ]; then
	DISTRIBUTION_CERTIFICATE="iPhone Distribution: MobileUp o.o.o."
else
	DISTRIBUTION_CERTIFICATE=$3
fi

#
if [ -z "$4" ]; then
	PROVISONNING_PROFILE="${HOME}/Documents/Provision/Super-AdHoc.mobileprovision"
else
	PROVISONNING_PROFILE=$4
fi


# DISTRIBUTION_CERTIFICATE="iPhone Developer: Dymov Eugene (48S9ZYFSAH)"
# PROVISONNING_PROFILE="${HOME}/Documents/Provision/TeamMobileUp.mobileprovision"
BUILD_HISTORY_DIR="${IPADIR}"

if [ -z "$5" ]; then
	IPANAME="${TARGETNAME}.ipa"
else
	IPANAME="$5.ipa"
fi

IPAFILE="${BUILD_HISTORY_DIR}/${IPANAME}"

if [ ! -d ${IPADIR} ]; then
	mkdir "${IPADIR}"
fi

# Action!
/usr/bin/xcrun -sdk iphoneos PackageApplication -v "${PROJECT_BUILDDIR}/${TARGETNAME}.app" -o "${IPAFILE}" --sign "$DISTRIBUTION_CERTIFICATE" --embed "${PROVISONNING_PROFILE}"  >> ${BUILDLOG} 2>&1

if [ $? != 0 ]
then
	echo "Archiving was not succeed. Check ${BUILDLOG}" 1>&2
	exit 2
else
	echo "${IPAFILE}"
	rm -rf ${BUILDDIR}
fi
