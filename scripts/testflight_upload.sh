API_TOKEN="61eb737a2b2a6dfe489ab1da40d4ff2a_MTM1NzY1MjAxMS0wOC0yMCAxNDoxMDoxMC41MDQ0MDA"

#Replace team token with MobileUp before uploading
TEAM_TOKEN="6f2760003b0a77538bbe6015135e1540_Njc1OTEyMDEyLTAzLTAyIDA4OjEzOjA2LjU0MzA2OA"

DATE=`date "+%Y-%m-%d_%H:%M:%S"`

GIT_REVISION=$(git log --pretty=oneline -1|cut -b 1-7)

NOTES="Date:${DATE}__Revision:${GIT_REVISION}"

DISTRIBUTION_LISTS="myriade"

curl http://testflightapp.com/api/builds.json -F file="@${1}" -F api_token=${API_TOKEN} -F team_token=${TEAM_TOKEN} -F notes="${NOTES}" -F distribution_lists=${DISTRIBUTION_LISTS} -F notify=False
